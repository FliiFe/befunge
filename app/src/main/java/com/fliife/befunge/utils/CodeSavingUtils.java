package com.fliife.befunge.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.ProtocolStringList;


public class CodeSavingUtils {
    public static void saveCode(String name, String code, Context context){
        byte[] currentlySavedByteArray = getBytesFromPrefs("saved_code", context);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SavedSamples.SavedCode toSave;
        if(currentlySavedByteArray == null){
            //Create object, and write it to storage
            toSave = SavedSamples.SavedCode.newBuilder()
                    .addName(name)
                    .addCode(code)
                    .build();
        }else{
            try{
                toSave = SavedSamples.SavedCode
                        .parseFrom(currentlySavedByteArray)
                        .toBuilder()
                        .addName(name)
                        .addCode(code)
                        .build();

            }catch(InvalidProtocolBufferException e){
                e.printStackTrace();
                return;
            }
        }
        byte[] serializedObject = toSave.toByteArray();
        String base64Object = new Base64().encode(serializedObject);
        prefs.edit().putString("saved_code", base64Object).apply();
    }

    public static String getCode(int position, Context context){
        byte[] currentlySavedByteArray = getBytesFromPrefs("saved_code", context);

        if(currentlySavedByteArray == null){
            return null;
        }
        try {

            return SavedSamples.SavedCode.parseFrom(currentlySavedByteArray).getCode(position);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void removeCode(int position, Context context){
        String[] names = getNamesArray(context);
        String[] codes = getCodesArray(context);
        if(names.length == 1){
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            prefs.edit().putString("saved_code", "").apply();
            return;
        }
        SavedSamples.SavedCode.Builder toSaveBuilder = SavedSamples.SavedCode.newBuilder();
        for (int i = 0; i < names.length; i++) {
            String name = names[i];
            if(i==position) continue;
            toSaveBuilder.addName(name);
        }
        for (int i = 0; i < codes.length; i++) {
            String code = codes[i];
            if(i==position) continue;
            toSaveBuilder.addCode(code);
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SavedSamples.SavedCode toSave = toSaveBuilder.build();
        byte[] serializedObject = toSave.toByteArray();
        String base64Object = new Base64().encode(serializedObject);
        prefs.edit().putString("saved_code", base64Object).apply();
    }

    public static String[] getNamesArray(Context context){
        byte[] currentlySavedByteArray = getBytesFromPrefs("saved_code", context);
        if(currentlySavedByteArray == null){
            return null;
        }
        try {
            ProtocolStringList list = SavedSamples.SavedCode.parseFrom(currentlySavedByteArray).getNameList();
            String[] names = new String[list.asByteStringList().toArray().length];
            for(int i=0; i<names.length; i++){
                names[i] = list.get(i);
            }
            return names;
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static String[] getCodesArray(Context context){
        byte[] currentlySavedByteArray = getBytesFromPrefs("saved_code", context);
        if(currentlySavedByteArray == null){
            return null;
        }
        try {
            ProtocolStringList list = SavedSamples.SavedCode.parseFrom(currentlySavedByteArray).getCodeList();
            String[] codes = new String[list.asByteStringList().toArray().length];
            for(int i=0; i<codes.length; i++){
                codes[i] = list.get(i);
            }
            return codes;
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static byte[] getBytesFromPrefs(String key, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String str =  prefs.getString(key, "");
        if(str == ""){
            return null;
        }
        byte[] result = new Base64().decode(str);
        return result;
    }
}
