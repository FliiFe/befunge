package com.fliife.befunge;


import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class LanguageSpecifications extends Activity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_specifications);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        // When swiping between pages, select the
                        // corresponding tab.
                        getActionBar().setSelectedNavigationItem(position);
                    }
                });

        final ActionBar actionBar = getActionBar();


        // Specify that tabs should be displayed in the action bar.
        assert actionBar != null;
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create a tab listener that is called when the user changes tabs.
        ActionBar.TabListener tabListener = new ActionBar.TabListener() {
            public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
                // hide the given tab
            }

            public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
                // probably ignore this event
            }
        };

        // Add 3 tabs, specifying the tab's text and TabListener
        for (int i = 0; i < 5; i++) {
            actionBar.addTab(actionBar.newTab().setText(mSectionsPagerAdapter.getPageTitle(i)).setTabListener(tabListener));
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_language_specifications, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_language_specifications, container, false);
            ListView listView = (ListView) rootView.findViewById(R.id.listView);
            int position = getArguments().getInt(ARG_SECTION_NUMBER);
            String[] chars = new String[0];
            String[] description = new String[0];

            switch(position-1){
                case 0:
                    //Maths
                    chars = new String[] {
                            "+",
                            "-",
                            "*",
                            "/",
                            "%"
                    };
                    description = new String[] {
                            "Addition: Pop two values a and b, then push the result of a+b",
                            "Subtraction: Pop two values a and b, then push the result of b-a",
                            "Multiplication: Pop two values a and b, then push the result of a*b",
                            "Integer division: Pop two values a and b, then push the result of b/a, rounded down. According to the specifications, if a is zero, ask the user what result they want.",
                            "Modulo: Pop two values a and b, then push the remainder of the integer division of b/a."
                    };
                    break;
                case 1:
                    //Stack
                    chars = new String[] {
                            "\"",
                            ":",
                            "\\",
                            "$",
                            ".",
                            ",",
                            "0 – 9"
                    };
                    description = new String[] {
                            "Toggle stringMode (push each character's ASCII value all the way up to the next \")",
                            "Duplicate top stack value",
                            "Swap top stack values",
                            "Pop (remove) top stack value and discard",
                            "Pop top of stack and output as integer",
                            "Pop top of stack and output as ASCII character",
                            "Push corresponding number onto the stack"
                    };
                    break;
                case 2:
                    //Move
                    chars = new String[] {
                            ">",
                            "<",
                            "^",
                            "v",
                            "?",
                            "#",
                            "@"
                    };
                    description = new String[] {
                            "PC direction right",
                            "PC direction left",
                            "PC direction up",
                            "PC direction down",
                            "Random PC direction",
                            "Bridge: jump over next command in the current direction of the current PC",
                            "End program"
                    };
                    break;
                case 3:
                    //Conditionals
                    chars = new String[] {
                            "!",
                            "`",
                            "_",
                            "|"
                    };
                    description = new String[] {
                            "Logical NOT: Pop a value. If the value is zero, push 1; otherwise, push zero.",
                            "Greater than: Pop two values a and b, then push 1 if b>a, otherwise zero.",
                            "Horizontal IF: pop a value; set direction to right if value=0, set to left otherwise",
                            "Vertical IF: pop a value; set direction to down if value=0, set to up otherwise"
                    };
                    break;
                case 4:
                    // I/O
                    chars = new String[] {
                            "g",
                            "p",
                            "&",
                            "~",
                            ".",
                            ","
                    };
                    description = new String[] {
                            "A \"get\" call (a way to retrieve data in storage). Pop two values y and x, then push the ASCII value of the character at that position in the program. If (x,y) is out of bounds, push 0",
                            "A \"put\" call (a way to store a value for later use). Pop three values y, x and v, then change the character at the position (x,y) in the program to the character with ASCII value v",
                            "Get integer from user and push it",
                            "Get character from user and push it",
                            "Pop top of stack and output as integer",
                            "Pop top of stack and output as ASCII character"
                    };
            }

            listView.setAdapter(new CustomAdapter(this.getActivity(), chars, description));

            return rootView;
        }
    }

    static class CustomAdapter extends ArrayAdapter<String>
    {
        public String[] chars;
        public String[] descriptions;
        public CustomAdapter(Context context, String[] chars, String[] descriptions)
        {
            super(context, R.layout.listview_layout, chars);
            this.chars = chars;
            this.descriptions = descriptions;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View view = inflater.inflate(R.layout.listview_layout, parent, false);
            String currentChar = chars[position];
            TextView textView1 = (TextView) view.findViewById(R.id.charView);
            TextView textView2 = (TextView) view.findViewById(R.id.charDesc);
            textView1.setText(currentChar);
            int p = position + 1;
            textView2.setText(descriptions[position]);
            return view;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "MATHS";
                case 1:
                    return "STACK";
                case 2:
                    return "MOVE";
                case 3:
                    return "CONDITIONALS";
                case 4:
                    return "I/O";
            }
            return null;
        }
    }
}
