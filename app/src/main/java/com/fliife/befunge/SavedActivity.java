package com.fliife.befunge;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.fliife.befunge.utils.CodeSavingUtils;

public class SavedActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved);
        final ListView savedListView  = (ListView) findViewById(R.id.savedListView);
        String[] values = CodeSavingUtils.getNamesArray(this);
        if(values == null){
            values = new String[] {"Nothing saved"};
        }
        savedListView.setAdapter(new ArrayAdapter<>(this, R.layout.layout_list_saved, R.id.textView2, values));
        savedListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = ((TextView) view.findViewById(R.id.textView2)).getText().toString();
                String code = CodeSavingUtils.getCode(position, getApplicationContext());
                Intent i = new Intent();
                i.putExtra("resultCode", code);
                setResult(RESULT_OK, i);
                finish();
            }
        });
        savedListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                String programName = "";
                try{
                    programName = CodeSavingUtils.getNamesArray(getApplicationContext())[position];
                }catch (NullPointerException e){
                    e.printStackTrace();
                    return true;
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(SavedActivity.this);

                builder.setTitle("Remove code")
                        .setMessage("Are you sure you want to remove the program '" + programName + "' ?");
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        CodeSavingUtils.removeCode(position, getApplicationContext());
                        Intent i = new Intent();
                        setResult(MainActivity.RESULT_RELOAD, i);
                        finish();
                    }
                });
                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Don't do anything
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();



                return true;
            }
        });
    }

}
